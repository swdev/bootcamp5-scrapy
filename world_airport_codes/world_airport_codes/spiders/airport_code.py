# -*- coding: utf-8 -*-
import scrapy


class AirportCodeSpider(scrapy.Spider):
    name = 'airport_code'
    allowed_domains = ['www.world-airport-codes.com']
    start_urls = ['http://www.world-airport-codes.com/']

    def parse(self, response):
        pass
